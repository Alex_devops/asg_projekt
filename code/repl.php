<?php

$array_from_to = [
  './?view=person' => "{{ route('person') }}",
  './?view=object' => "{{ route('object') }}",
  './?view=teamsearch' => "{{ route('teamSearch') }}",
  './?view=team' => "{{ route('team') }}",
  './?view=article' => "{{ route('article') }}",
  'index.php' => "{{ route('home') }}",
  'admin.blade.php?view=saveobject' => "{{ route('saveObject') }}",
  'admin.blade.php?view=myobjects' => "{{ route('myObjects') }}",
  'admin.blade.php?view=saveteam' => "{{ route('saveTeam') }}",
  'admin.blade.php?view=profile' => "{{ route('profile') }}",
  'admin.blade.php?view=cities' => "{{ route('cities.index') }}",
  'admin.blade.php' => "{{ route('adminHome') }}",
];


$it = new RecursiveDirectoryIterator(__DIR__);


foreach(new RecursiveIteratorIterator($it) as $file)
{
    if($file != __DIR__.'/repl.php' && !is_dir($file))
    {
        $content = file_get_contents($file);

        $content = str_replace(array_keys($array_from_to), $array_from_to, $content);

        file_put_contents($file, $content);
    }
}
