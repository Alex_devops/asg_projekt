<?php


namespace App\AsgEvent\Repositories;

use App\AsgEvent\Interfaces\FrontendRepositoryInterface;
use App\{PlayerObject,City,Team,Reservation,Article,User,Comment};


class FrontendRepository implements FrontendRepositoryInterface  {


    public function getObjectsForMainPage()
    {
        return PlayerObject::with(['city','photos'])->ordered()->paginate(8);
    }

    public function getObject($id)
    {
        // teams.object.city   for json mobile because there is no lazy loading there
        return  PlayerObject::with(['city','photos', 'address','users.photos','teams.photos','comments.user','articles.user','teams.object.city'])->find($id);
    }

    public function getSearchCities( string $term)
    {
        return  City::where('name', 'LIKE', $term . '%')->get();
    }

    public function getSearchResults( string $city)
    {
        // teams.object.photos  for json mobile
        return  City::with(['teams.reservations','teams.photos','teams.object.photos'])->where('name',$city)->first() ?? false;
    }


    public function getTeam($id)
    {
        // with - for mobile json
        return  team::with(['object.address'])->find($id);
    }

    public function getReservationsByTeamId( $team_id )
    {
        return  Reservation::where('team_id',$team_id)->get();
    }

    public function getArticle($id)
    {
        return  Article::with(['object.photos','comments'])->find($id);
    }

    public function getPerson($id)
    {
        return  User::with(['objects','larticles','comments.commentable'])->find($id);
    }

    public function like($likeable_id, $type, $request)
    {
        $likeable = $type::find($likeable_id);

        return $likeable->users()->attach($request->user()->id);
    }

    public function unlike($likeable_id, $type, $request)
    {
        $likeable = $type::find($likeable_id);

        return $likeable->users()->detach($request->user()->id);
    }

    public function addComment($commentable_id, $type, $request)
    {
        $commentable = $type::find($commentable_id);

        $comment = new Comment;

        $comment->content = $request->input('content');

        $comment->rating = $type == 'App\PlayerObject' ? $request->input('rating') : 0;

        $comment->user_id = $request->user()->id;

        return $commentable->comments()->save($comment);
    }

    public function makeReservation($team_id, $city_id, $request)
    {
        return Reservation::create([
                'user_id'=>$request->user()->id,
                'city_id'=>$city_id,
                'team_id'=>$team_id,
                'status'=>0,
                'day_in'=>date('Y-m-d', strtotime($request->input('checkin'))),
                'day_out'=>date('Y-m-d', strtotime($request->input('checkout')))
            ]);
    }

}


