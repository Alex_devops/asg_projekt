<?php

namespace App\AsgEvent\Repositories;

use App\AsgEvent\Interfaces\BackendRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use App\{PlayerObject, Reservation, City, User, Photo, Address, Article, Team, Notification};


class BackendRepository implements BackendRepositoryInterface  {


    public function getOwnerReservations($request)
    {
        return PlayerObject::with([

                  'teams' => function($q) {
                        $q->has('reservations'); // works like where clause for team
                    }, // give me teams only with reservations, if it wasn't there would be teams without reservations

                    'teams.reservations.user'

                  ])
                    ->has('teams.reservations') // ensures that it gives me only those objects that have at least one reservation, has() here works like where clause for Object
                    ->where('user_id', $request->user()->id)
                    ->get();
    }


    public function getPlayerReservations($request)
    {

       return PlayerObject::with([

                    'teams.reservations' => function($q) use($request) { // filters reserervations of other users

                            $q->where('user_id',$request->user()->id);

                    },

                    'teams'=>function($q) use($request){
                        $q->whereHas('reservations',function($query) use($request){
                            $query->where('user_id',$request->user()->id);
                        });
                    },

                    'teams.reservations.user'

                  ])

                    ->whereHas('teams.reservations',function($q) use($request){  // acts like has() with additional conditions

                        $q->where('user_id',$request->user()->id);

                    })
                    ->get();
    }

    public function getReservationData($request)
    {
        return  Reservation::with('user', 'team')
                ->where('team_id', $request->input('team_id'))
                ->where('day_in', '<=', date('Y-m-d', strtotime($request->input('date'))))
                ->where('day_out', '>=', date('Y-m-d', strtotime($request->input('date'))))
                ->first();
    }


    public function getReservation($id)
    {
        return Reservation::find($id);
    }



    public function deleteReservation(Reservation $reservation)
    {
        return $reservation->delete();
    }


    public function confirmReservation(Reservation $reservation)
    {
        return $reservation->update(['status' => true]);
    }

    public function getCities()
    {
        return City::orderBy('name','asc')->get();
    }

    public function getCity($id)
    {
        return City::find($id);
    }

    public function createCity($request)
    {
        return City::create([
            'name' => $request->input('name')
        ]);
    }

    public function updateCity($request, $id)
    {
        return City::where('id',$id)->update([
            'name' => $request->input('name')
        ]);
    }

    public function deleteCity($id)
    {
        return City::where('id',$id)->delete();
    }

    public function saveUser($request)
    {
        $user = User::find($request->user()->id);
        $user->name = $request->input('name');
        $user->surname = $request->input('surname');
        $user->email = $request->input('email');
        $user->save();

        return $user;
    }


    public function getPhoto($id)
    {
        return Photo::find($id);
    }


    public function updateUserPhoto(User $user,Photo $photo)
    {
        return $user->photos()->save($photo);
    }

    public function createUserPhoto($user,$path)
    {
        $photo = new Photo;
        $photo->path = $path;
        $user->photos()->save($photo);
    }

    public function deletePhoto(Photo $photo)
    {
        $path = $photo->storagepath;
        $photo->delete();
        return $path;
    }

    public function getObject($id)
    {
        return PlayerObject::find($id);
    }


    public function updateObjectWithAddress($id, $request)
    {

        Address::where('object_id',$id)->update([
            'street'=>$request->input('street'),
            'number'=>$request->input('number'),
            ]);

        $object = PlayerObject::find($id);


        $object->name = $request->input('name');
        $object->city_id = $request->input('city');
        $object->description = $request->input('description');

        $object->save();

        return $object;

    }


    public function createNewObjectWithAddress($request)
    {
        $object = new PlayerObject;
        $object->user_id = $request->user()->id;

        $object->name = $request->input('name');
        $object->city_id = $request->input('city');
        $object->description = $request->input('description');

        $object->save();


        $address = new Address;
        $address->street = $request->input('street');
        $address->number = $request->input('number');
        $address->object_id = $object->id;
        $address->save();
        $object->address()->save($address);

        return $object;
    }

    public function saveObjectPhotos(PlayerObject $object, string $path)
    {

        $photo = new Photo;
        $photo->path = $path;
        return $object->photos()->save($photo);

    }

    public function saveArticle($object_id,$request)
    {
            return Article::create([
            'title' => $request->input('title'),
            'content' => $request->input('content'),
            'user_id' => $request->user()->id,
            'object_id' =>$object_id,
            'created_at' => new \DateTime(),
        ]);
    }

    public function getArticle($id)
    {
        return Article::find($id);
    }


    public function deleteArticle(Article $article)
    {
        return  $article->delete();
    }


    public function getMyObjects($request)
    {
        return PlayerObject::where('user_id',$request->user()->id)->get();
    }


    public function deleteObject($id)
    {
        return PlayerObject::where('id',$id)->delete();
    }

    public function getTeam($id)
    {
        return Team::find($id);
    }

    public function updateTeam($id,$request)
    {
        $team = Team::find($id);
        $team->team_number = $request->input('team_number');
        $team->team_size = $request->input('team_size');
        $team->price = $request->input('price');
        $team->description = $request->input('description');

        $team->save();

        return $team;
    }

    public function createNewTeam($request)
    {
        $team = new Team;
        $object = PlayerObject::find( $request->input('object_id') );
        $team->object_id = $request->input('object_id') ;

        $team->team_number = $request->input('team_number');
        $team->team_size = $request->input('team_size');
        $team->price = $request->input('price');
        $team->description = $request->input('description');

        $team->save();

        $object->teams()->save($team);

        return $team;
    }

    public function saveTeamPhotos(Team $team, string $path)
    {
        $photo = new Photo;
        $photo->path = $path;
        return $team->photos()->save($photo);
    }

    public function deleteTeam(Team $team)
    {
        return $team->delete();
    }

    public function setReadNotifications($request)
    {
       return Notification::where('id', $request->input('id'))
                        ->update(['status' => 1]);
    }

    public function getUserNotifications($id)
    {
        return Notification::where('user_id', $id)->where('shown', 0)->get();
    }

    public function setShownNotifications($request)
    {
        return Notification::whereIn('id', $request->input('idsOfNotShownNotifications'))
                        ->update(['shown' => 1]);
    }

    public function getNotifications()
    {
        return Notification::where('user_id', Auth::user()->id )->where('status',0)->get(); // for mobile
    }
}


