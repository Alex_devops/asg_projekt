<?php


namespace App\AsgEvent\Interfaces;



interface FrontendRepositoryInterface   {


    public function getObjectsForMainPage();


    public function getObject($id);
}


