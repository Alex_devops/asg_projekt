<?php
namespace App\AsgEvent\Presenters;


trait UserPresenter {


    public function getFullNameAttribute()
    {
        return $this->name.' '.$this->surname;
    }

}

