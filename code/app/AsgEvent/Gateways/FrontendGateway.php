<?php
namespace App\AsgEvent\Gateways;

use App\AsgEvent\Interfaces\FrontendRepositoryInterface;


class FrontendGateway {

    use \Illuminate\Foundation\Validation\ValidatesRequests;

    public function __construct(FrontendRepositoryInterface $fR )
    {
        $this->fR = $fR;
    }



    public function searchCities($request)
    {
        $term = $request->input('term');

        $results = array();

        $queries = $this->fR->getSearchCities($term);

        foreach ($queries as $query)
        {
            $results[] = ['id' => $query->id, 'value' => $query->name];
        }

        return $results;
    }

    public function getSearchResults($request)
    {

        $request->flash(); // inputs for session for one request

        if( $request->input('city') != null)
        {

            $dayin = date('Y-m-d', strtotime($request->input('check_in')));
            $dayout = date('Y-m-d', strtotime($request->input('check_out')));

            $result = $this->fR->getSearchResults($request->input('city'));

            if($result)
            {


                foreach ($result->teams as $k=>$team)
                {
                   if( (int) $request->input('team_size') > 0 )
                   {
                        if($team->team_size != $request->input('team_size'))
                        {
                            $result->teams->forget($k);
                        }
                   }

                    foreach($team->reservations as $reservation)
                    {

                        if( $dayin >= $reservation->day_in
                            &&  $dayin <= $reservation->day_out
                        )
                        {
                            $result->teams->forget($k);
                        }
                        elseif( $dayout >= $reservation->day_in
                            &&  $dayout <= $reservation->day_out
                        )
                        {
                            $result->teams->forget($k);
                        }
                        elseif( $dayin <= $reservation->day_in
                            &&  $dayout >= $reservation->day_out
                        )
                        {
                            $result->teams->forget($k);
                        }

                    }

                }


                if(count($result->teams)> 0)
                return $result;  // filtered result
                else return false;

            }

        }

        return false;

    }

    public function addComment($commentable_id, $type, $request)
    {
        $this->validate($request,[
            'content'=>"required|string"
        ]);

        return $this->fR->addComment($commentable_id, $type, $request);
    }

    public function checkAvaiableReservations($team_id, $request)
    {

        $dayin = date('Y-m-d', strtotime($request->input('checkin')));
        $dayout = date('Y-m-d', strtotime($request->input('checkout')));

        $reservations = $this->fR->getReservationsByTeamId($team_id);

        $avaiable = true;
        foreach($reservations as $reservation)
        {
            if( $dayin >= $reservation->day_in
                &&  $dayin <= $reservation->day_out
            )
            {
                $avaiable = false;break;
            }
            elseif( $dayout >= $reservation->day_in
                &&  $dayout <= $reservation->day_out
            )
            {
                $avaiable = false;break;
            }
            elseif( $dayin <= $reservation->day_in
                &&  $dayout >= $reservation->day_out
            )
            {
                $avaiable = false;break;
            }
        }

        return $avaiable;
    }



    public function makeReservation($team_id, $city_id, $request)
    {
        $this->validate($request,[
            'checkin'=>"required|string",
            'checkout'=>"required|string"
        ]);

        return $this->fR->makeReservation($team_id, $city_id, $request);
    }

}


