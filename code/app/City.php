<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class City extends Model
{
    //protected $table = 'table_name';
    protected $guarded = [];
    public $timestamps = false;

    public function teams()
    {
        return $this->hasManyThrough('App\Team', 'App\PlayerObject','city_id','object_id');
    }

}

