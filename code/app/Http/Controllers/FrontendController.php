<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AsgEvent\Interfaces\FrontendRepositoryInterface;
use App\AsgEvent\Gateways\FrontendGateway;
use App\Events\OrderPlacedEvent;

class FrontendController extends Controller
{

    public function __construct(FrontendRepositoryInterface $frontendRepository, FrontendGateway $frontendGateway )
    {

        $this->middleware('auth')->only(['makeReservation','addComment','like','unlike']);

        $this->fR = $frontendRepository;
        $this->fG = $frontendGateway;
    }



    public function index()
    {
        $objects = $this->fR->getObjectsForMainPage();
        //dd($objects);
        return view('frontend.index',['objects'=>$objects]);
    }

    public function article($id)
    {
        $article = $this->fR->getArticle($id);
        return view('frontend.article',compact('article'));
    }

    public function object($id)
    {
        $object = $this->fR->getObject($id);

        return view('frontend.object',['object'=>$object]);
    }


    public function person($id)
    {
        $user = $this->fR->getPerson($id);
        return view('frontend.person', ['user'=>$user]);
    }


    public function team($id)
    {
        $team = $this->fR->getTeam($id);
        return view('frontend.team',['team'=>$team]);
    }


    public function ajaxGetTeamReservations($id)
    {

        $reservations = $this->fR->getReservationsByTeamId($id);

        return response()->json([
            'reservations'=>$reservations
        ]);
    }


    public function teamsearch(Request $request )
    {
        if($city = $this->fG->getSearchResults($request))
        {
            return view('frontend.teamsearch',['city'=>$city]);
        }
        else
        {
            if (!$request->ajax())
            return redirect('/')->with('noteams', __('No offers were found matching the criteria'));
        }

    }


    public function searchCities(Request $request)
    {

        $results = $this->fG->searchCities($request);

        return response()->json($results);
    }

    public function like($likeable_id, $type, Request $request)
    {
        $this->fR->like($likeable_id, $type, $request);

        return redirect()->back();
    }


    public function unlike($likeable_id, $type, Request $request)
    {
        $this->fR->unlike($likeable_id, $type, $request);

        return redirect()->back();
    }

    public function addComment($commentable_id, $type, Request $request)
    {
        $this->fG->addComment($commentable_id, $type, $request);

        return redirect()->back();
    }

    public function makeReservation($team_id, $city_id, Request $request)
    {

        $avaiable = $this->fG->checkAvaiableReservations($team_id, $request);

        if(!$avaiable)
        {
            if (!$request->ajax())
            {
                $request->session()->flash('reservationMsg', __('There are no vacancies'));
                return redirect()->route('team',['id'=>$team_id,'#reservation']);
            }

            return response()->json(['reservation'=>false]);
        }
        else
        {
            $reservation = $this->fG->makeReservation($team_id, $city_id, $request);

            event( new OrderPlacedEvent($reservation) );

            if (!$request->ajax())
            return redirect()->route('adminHome');
            else
            return response()->json(['reservation'=>$reservation]);
        }

    }

}

