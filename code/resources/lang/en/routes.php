<?php
return [


    'article' => 'article',
    'object' => 'object',
    'person' => 'person',
    'team' => 'team',
    'teamsearch' => 'teamsearch',
    'myobjects' => 'myobjects',
    'saveobject' => 'saveobject',
    'profile' => 'profile',
    'saveteam' => 'saveteam',
    'deleteobject' => 'deleteobject',
    'deleteteam' => 'deleteteam',

];
