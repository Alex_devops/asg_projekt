<?php

return [


    'article' => 'artykul',
    'object' => 'obiekt',
    'person' => 'osoba',
    'team' => 'druzyna',
    'teamsearch' => 'szukajdruzyny',
    'myobjects' => 'mojeobiekty',
    'saveobject' => 'zapiszobjekt',
    'profile' => 'profil',
    'saveteam' => 'zapiszdruzyne',
    'deleteobject' => 'usunobiekt',
    'deleteteam' => 'usundruzyne',

];

