
@extends('layouts.frontend')

@section('content')
<div class="container-fluid places">

    <h1 class="text-center">Available teams</h1>

    @foreach( $city->teams->chunk(4) as $chunked_teams )

        <div class="row">

            @foreach( $chunked_teams as $team )

                <div class="col-md-3 col-sm-6">

                    <div class="thumbnail">
                        <img class="img-responsive img-circle" src="{{ $team->photos->first()->path ?? $placeholder  }}" alt="...">
                        <div class="caption">
                            <h3>Nr {{ $team->team_number  }} <small class="orange bolded">{{ $team->price  }}$</small> </h3>
                            <p>{{ str_limit($team->description,80)  }}</p>
                            <p><a href="{{ route('team',['id'=>$team->id]) }}" class="btn btn-primary" role="button">Details</a><a href="{{ route('team',['id'=>$team->id]) }}#reservation" class="btn btn-success pull-right" role="button">Reservation</a></p>
                        </div>
                    </div>
                </div>

            @endforeach


        </div>

    @endforeach

</div>
@endsection


