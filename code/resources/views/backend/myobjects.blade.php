@extends('layouts.backend')

@section('content')
<h2>List of objects</h2>
@foreach( $objects as $object )

    <div class="panel panel-success top-buffer">
        <div class="panel-heading">
            <h3 class="panel-title">{{ $object->name }} object <small><a href="{{ route('saveObject',['id'=>$object->id]) }}" class="btn btn-danger btn-xs">edit</a> <a href="{{ route('saveTeam').'?object_id='.$object->id }}" class="btn btn-danger btn-xs">Add a team</a> <a title="delete" href="{{ route('deleteObject',['id'=>$object->id])}}"><span class="glyphicon glyphicon-remove"></span></a></small> </h3>
        </div>

        <div class="panel-body">
            @foreach( $object->teams as $team )
                <span class="my_objects">
                    Team {{ $team->team_number}} <a title="edit" href="{{ route('saveTeam',['id'=>$team->id]) }}"><span class="glyphicon glyphicon-edit"></span></a> <a title="delete" href="{{ route('deleteTeam',['id'=>$team->id]) }}"><span class="glyphicon glyphicon-remove"></span></a> </span>
            @endforeach
        </div>

    </div>

@endforeach
@endsection



