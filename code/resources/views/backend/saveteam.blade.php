
@extends('layouts.backend')

@section('content')

@if( $team ?? false )
<h2>Editing the team of the {{ $team->object->name }} object</h2>
@else
<h2>Adding a new team to the object</h2>
@endif

<form {{ $novalidate }} action="{{ route('saveTeam',['id'=>$team->id ?? false])}}" method="POST" enctype="multipart/form-data" class="form-horizontal">
    <fieldset>
        <div class="form-group">
            <label for="teamNumber" class="col-lg-2 control-label">team number *</label>
            <div class="col-lg-10">
                <input name="team_number" value="{{ $team->team_number ?? old('team_number') }}" required type="number" class="form-control" id="teamNumber" placeholder="">
            </div>
        </div>
        <div class="form-group">
            <label for="peopleNumber" class="col-lg-2 control-label">Team size *</label>
            <div class="col-lg-10">
                <input name="team_size" value="{{ $team->team_size ?? old('team_size') }}" required type="number" class="form-control" id="peopleNumber" placeholder="">
            </div>
        </div>
        <div class="form-group">
            <label for="price" class="col-lg-2 control-label">Price *</label>
            <div class="col-lg-10">
                <input name="price" value="{{ $team->price ?? old('price') }}" required type="number" class="form-control" id="price" placeholder="">
            </div>
        </div>
        <div class="form-group">
            <label for="descr" class="col-lg-2 control-label">team description *</label>
            <div class="col-lg-10">
                <textarea name="description" required class="form-control" rows="3" id="descr">{{ $team->description ?? old('description') }}</textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <label for="teamPictures">team gallery</label>
                <input type="file" name="teamPictures[]" id="teamPictures" multiple>
                <p class="help-block">Add a photo gallery of the place</p>
            </div>
        </div>

        @if( $team ?? false )
        <div class="col-lg-10 col-lg-offset-2">

            @foreach( $team->photos->chunk(4) as $chunked_photos )

                <div class="row">


                    @foreach( $chunked_photos as $photo )

                        <div class="col-md-3 col-sm-6">
                            <div class="thumbnail">
                                <img class="img-responsive" src="{{ $photo->path ?? $placeholder }}" alt="...">
                                <div class="caption">
                                    <p><a href="{{ route('deletePhoto',['id'=>$photo->id]) }}" class="btn btn-primary btn-xs" role="button">Delete</a></p>
                                </div>

                            </div>
                        </div>

                    @endforeach

                </div>


             @endforeach


        </div>
        @endif


        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <button type="submit" class="btn btn-primary">Save team</button>
            </div>
        </div>

    </fieldset>
    <input type="hidden" name="object_id" value="{{ $object_id ?? null }}">
    {{ csrf_field() }}
</form>
@endsection


