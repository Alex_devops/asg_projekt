<?php


use Illuminate\Database\Seeder;

class TeamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create();


        for ($i = 1; $i <= 30; $i++) {

            DB::table('teams')->insert([
                'team_number' => $faker->unique()->numberBetween(1, 30),
                'team_size' => $faker->numberBetween(1, 5),
                'price' => $faker->numberBetween(100, 600),
                'description' => $faker->text(1000),
                'object_id' => $faker->numberBetween(1, 10),

            ]);
        }
    }
}

