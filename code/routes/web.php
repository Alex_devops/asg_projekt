<?php



Route::get('/','FrontendController@index')->name('home');
Route::get(trans('routes.object').'/{id}','FrontendController@object')->name('object');
Route::post(trans('routes.teamsearch'),'FrontendController@teamsearch')->name('teamSearch');
Route::get(trans('routes.team').'/{id}','FrontendController@team')->name('team');
Route::get(trans('routes.article').'/{id}','FrontendController@article')->name('article');
Route::get(trans('routes.article'),'FrontendController@article')->name('article');
Route::get(trans('routes.person').'/{id}','FrontendController@person')->name('person');

Route::get('/searchCities', 'FrontendController@searchCities');
Route::get('/ajaxGetTeamReservations/{id}', 'FrontendController@ajaxGetteamReservations');

Route::get('/like/{likeable_id}/{type}', 'FrontendController@like')->name('like');
Route::get('/unlike/{likeable_id}/{type}', 'FrontendController@unlike')->name('unlike');

Route::post('/addComment/{commentable_id}/{type}', 'FrontendController@addComment')->name('addComment');
Route::post('/makeReservation/{team_id}/{city_id}', 'FrontendController@makeReservation')->name('makeReservation');

Route::group(['prefix'=>'admin','middleware'=>'auth'],function(){

    Route::get('/getNotifications', 'BackendController@getNotifications');
    Route::post('/setReadNotifications', 'BackendController@setReadNotifications');
    Route::get('/','BackendController@index')->name('adminHome');
    Route::get(trans('routes.myobjects'),'BackendController@myobjects')->name('myObjects');
    Route::match(['GET','POST'],trans('routes.saveobject').'/{id?}','BackendController@saveObject')->name('saveObject');
    Route::match(['GET','POST'],trans('routes.profile'),'BackendController@profile')->name('profile');
    Route::get('/deletePhoto/{id}', 'BackendController@deletePhoto')->name('deletePhoto');
    Route::match(['GET','POST'],trans('routes.saveteam').'/{id?}', 'BackendController@saveTeam')->name('saveTeam');
    Route::get(trans('routes.deleteteam').'/{id}', 'BackendController@deleteTeam')->name('deleteTeam');
    Route::get('/cities','BackendController@cities')->name('cities.index');

    Route::get('/deleteArticle/{id}', 'BackendController@deleteArticle')->name('deleteArticle');
    Route::post('/saveArticle/{id?}', 'BackendController@saveArticle')->name('saveArticle');

    Route::get('/ajaxGetReservationData', 'BackendController@ajaxGetReservationData');
    Route::get('/ajaxSetReadNotification', 'BackendController@ajaxSetReadNotification');
    Route::get('/ajaxGetNotShownNotifications', 'BackendController@ajaxGetNotShownNotifications');
    Route::get('/ajaxSetShownNotifications', 'BackendController@ajaxSetShownNotifications');

    Route::get('/confirmReservation/{id}', 'BackendController@confirmReservation')->name('confirmReservation');
    Route::get('/deleteReservation/{id}', 'BackendController@deleteReservation')->name('deleteReservation');

    Route::resource('cities', 'CityController');

    Route::get(trans('routes.deleteobject').'/{id}', 'BackendController@deleteObject')->name('deleteObject');
});


Auth::routes();



