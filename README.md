# Prerequisites:
Vagrant and VirtualBox installed!

# More Information:
https://laravel.com/docs/9.x/homestead#:~:text=%20Laravel%20Homestead%20%201%20Introduction.%20Laravel%20strives,...%20These%20commands%20pull%20the%20latest...%20More%20

# Instruction
Source code is located in ./code directory.

All Vagrant commands must be run in the context of HOMESTEAD directory, where is a Vagrantfile located.

Run:
```
vagrant up
```

Wait a while and check application in your host:
http://localhost:8000


Sync directories ./code/ => /home/vagrant/code between Windows and Linux Virtual Machine automatically:
Run in a new CMD:
```
vagrant rsync-auto
```

Done. You can develop your code on Windows Machine!

# Connecting to database
* Host Machine: localhost:33060 -> Address and port for Host Machine. You can use some viewers, for example https://www.mysql.com/products/workbench/ on your Host Machine.
* Virtual Machine: localhost:3306 -> Application(Laravel) should use this address and port to connect to database in the code.

The username and password is homestead / secret.

More information: https://laravel.com/docs/9.x/homestead#connecting-to-databases

# Bootstraping project
You do not need to do this again!
This was done at the beginning.
Just for your information.

Log in to machine:
```
vagrant ssh
```

Run commands:
```
mkdir /home/vagrant/code
cd /home/vagrant/code
composer create-project laravel/laravel . 5.8
php repl.php
```
